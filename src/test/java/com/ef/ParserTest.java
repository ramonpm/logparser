package com.ef;

import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class ParserTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *          name of the test case
	 */
	public ParserTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(ParserTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testeGivenData1() {
		Params params = new Params(new String[] { "--accesslog=/home/ramonpm/Downloads/Java_MySQL_Test/access.log",
				"--startDate=2017-01-01.15:00:00", "--duration=hourly", "--threshold=200" });
		Parser parser = new Parser();
		parser.parse(params);
		Map<String, Integer> blockedIPs = parser.getLogAnalysis().getBlockedIPs();
		assertTrue(blockedIPs.containsKey("192.168.11.231"));
	}
	
	public void testeGivenData2() {
		Params params = new Params(new String[] { "--accesslog=/home/ramonpm/Downloads/Java_MySQL_Test/access.log",
				"--startDate=2017-01-01.00:00:00", "--duration=daily", "--threshold=500" });
		Parser parser = new Parser();
		parser.parse(params);
		Map<String, Integer> blockedIPs = parser.getLogAnalysis().getBlockedIPs();
		assertTrue(blockedIPs.containsKey("192.168.102.136"));
	}
}
