package com.ef;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BlackListService {

	public void save(Map<String, Integer> blockedIPs) {
		Connection con = null;
		Statement st = null;

		try {

			String batchValues = batchValuesOf(blockedIPs);

			con = DriverManager.getConnection(DBConfig.JDBC_URL, DBConfig.USER, DBConfig.PASSWORD);
			st = con.createStatement();

			String batchInsert = "INSERT INTO BLACKLIST(ip, reason) VALUES " + batchValues;

			st.executeUpdate(batchInsert);

		} catch (SQLException ex) {

			Logger lgr = Logger.getLogger(RequestService.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

		} finally {

			try {

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {

				Logger lgr = Logger.getLogger(RequestService.class.getName());
				lgr.log(Level.SEVERE, ex.getMessage(), ex);
			}
		}
	}

	private String batchValuesOf(Map<String, Integer> blockedIPs) {
		StringBuilder result = new StringBuilder();

		String prefix = "";

		for (Map.Entry<String, Integer> entry : blockedIPs.entrySet()) {
			result.append(prefix);
			prefix = ",";
			result.append("(");
			result.append(entry.getKey() + ",");
			result.append(entry.getValue() + " requests");
			result.append(")");
		}

		return result.toString();
	}

}
