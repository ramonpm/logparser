package com.ef;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Request {

	private LocalDateTime date;

	private String ip;

	private String type;

	private Integer status;

	private String client;

	public Request(String[] data) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		LocalDateTime localDateTime = LocalDateTime.parse(data[0], formatter);
		this.date = localDateTime;
		this.ip = data[1];
		this.type = data[2];
		this.status = Integer.valueOf(data[3]);
		this.client = data[4];
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

}
