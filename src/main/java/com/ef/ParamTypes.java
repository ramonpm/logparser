package com.ef;

public enum ParamTypes {
	STARTDATE("startDate"), DURATION("duration"), THRESHOLD("threshold"), ACCESSLOG("accesslog");
	
	private String value;
	
	ParamTypes(String type) {
		this.value = type;
	}

	public String getValue() {
		return value;
	}
	
	public static ParamTypes fromName(String name) {
		return ParamTypes.valueOf(name.toUpperCase());
	}
}
