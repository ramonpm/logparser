package com.ef;

public enum DurationType {
	HOURLY, DAILY;

	public static DurationType fromName(String name) {
		return DurationType.valueOf(name.toUpperCase());
	}
}
