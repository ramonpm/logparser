package com.ef;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Params {
	
	private DurationType durationType;
	
	private LocalDateTime startDate;
	
	private Integer threshold;

	private String accesslog;
	
	public Params(String[] args) {
		for (String arg : args) {
			String[] key = arg.replaceFirst("--", "").split("=");
			
			switch (ParamTypes.fromName(key[0])) {
			case DURATION:
				this.setDurationType(DurationType.fromName(key[1]));
				break;

			case STARTDATE:
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss");
				this.startDate = LocalDateTime.parse(key[1], formatter);
				break;

			case THRESHOLD:
				this.threshold = Integer.valueOf(key[1]);
				break;
				
			case ACCESSLOG:
				this.accesslog = key[1];
				break;

			default:
				break;
			}
		}
	}

	public DurationType getDurationType() {
		return durationType;
	}

	public void setDurationType(DurationType durationType) {
		this.durationType = durationType;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	public String getAccesslog() {
		return accesslog;
	}

	public void setAccesslog(String accesslog) {
		this.accesslog = accesslog;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

}
