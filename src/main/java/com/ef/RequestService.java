package com.ef;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RequestService {

	public void save(List<Request> requests) {
		Connection con = null;
		Statement st = null;

		try {

			String batchValues = batchValuesOf(requests);

			con = DriverManager.getConnection(DBConfig.JDBC_URL, DBConfig.USER, DBConfig.PASSWORD);
			st = con.createStatement();

			String batchInsert = "INSERT INTO REQUEST(created_at, ip, type, status, client) VALUES " + batchValues;
			
			st.executeUpdate(batchInsert);

		} catch (SQLException ex) {

			Logger lgr = Logger.getLogger(RequestService.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

		} finally {

			try {

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {

				Logger lgr = Logger.getLogger(RequestService.class.getName());
				lgr.log(Level.SEVERE, ex.getMessage(), ex);
			}
		}
	}

	private String batchValuesOf(List<Request> requests) {
		StringBuilder result = new StringBuilder();
		
		String prefix = "";
		for (Request request : requests) {
			result.append(prefix);
			prefix = ",";
			result.append("(");
			result.append(request.getDate() + ",");
			result.append(request.getIp() + ",");
			result.append(request.getType() + ",");
			result.append(request.getStatus() + ",");
			result.append(request.getClient());
			result.append(")");
		}
		
		return result.toString();
	}

}
