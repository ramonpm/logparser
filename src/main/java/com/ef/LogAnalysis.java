package com.ef;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class LogAnalysis {

	private Map<String, Integer> blockedIPs;
	
	private List<Request> requests;

	public LogAnalysis() {
		this.blockedIPs = new HashMap<>();
		this.requests = new ArrayList<>();
	}

	public LogAnalysis of(Params params) {
		Map<String, Integer> ips = new HashMap<>();
		try (Stream<String> stream = Files.lines(Paths.get(params.getAccesslog()))) {
			stream.forEach(line -> {
				String[] data = line.split("\\|");
				Request request = new Request(data);

				ChronoUnit chronoUnit = params.getDurationType() == DurationType.DAILY ? ChronoUnit.DAYS : ChronoUnit.HOURS;

				if (request.getDate().isAfter(params.getStartDate())
						&& request.getDate().isBefore(params.getStartDate().plus(1, chronoUnit))) {
					Integer lastValue = ips.get(data[1]) == null ? 0 : ips.get(data[1]);
					ips.put(data[1], lastValue + 1);
					
					requests.add(request);

					if (ips.get(data[1]) >= params.getThreshold()) {
						blockedIPs.put(data[1], ips.get(data[1]));
					}
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public Map<String, Integer> getBlockedIPs() {
		return blockedIPs;
	}

	public void setBlockedIPs(Map<String, Integer> blockedIPs) {
		this.blockedIPs = blockedIPs;
	}

	public List<Request> getRequests() {
		return this.requests;
	}

}
