package com.ef;

public class Parser {

	private LogAnalysis logAnalysis;
	
	private RequestService requestService;
	
	private BlackListService blackListService;
	
	public Parser() {
		this.logAnalysis = new LogAnalysis();
		this.requestService = new RequestService();
		this.blackListService = new BlackListService();
	}
	
	public void parse(Params params) {
		this.logAnalysis.of(params);
		this.requestService.save(this.logAnalysis.getRequests());
		this.blackListService.save(this.logAnalysis.getBlockedIPs());
	}
	
	public LogAnalysis getLogAnalysis() {
		return this.logAnalysis;
	}

	public static void main(String[] args) {

	}
}
